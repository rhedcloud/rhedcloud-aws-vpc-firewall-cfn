'''
----------------------------
test_rhedcloud_vpn1_tunnel.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn1_tunnel",
"Description": "Verify the tunnnel inside cidr in the VPN setup.",
"Plan": "Describe the VPN connection connecting to AWS Research VPC VPN Endpoint 1, by using tags, and check tunnnel inside cidr.",
"ExpectedResult": "Success"
'''

import pytest
import ipaddress
import xml.etree.ElementTree as ET
from aws_test_functions import aws_client, get_vpc_ids
from ipaddress import ip_network

def check_inside_tunnels(xml, gw, network_1, network_2):
    path = './ipsec_tunnel/' + gw + '/tunnel_inside_address/ip_address'
    list = xml.findall(path)
    if len(list) != 2:
        return False
    ip1 = ipaddress.ip_network(list[0].text)
    ip2 = ipaddress.ip_network(list[1].text)
    if network_1.overlaps(ip1):
        return network_2.overlaps(ip2)
    elif network_1.overlaps(ip2):
        return network_2.overlaps(ip1)
    else:
        return False

@aws_client('ec2')
def check_vpn_connection_tunnels(vpc_id, vpn_tag_name, tunnel_cidr_1, tunnel_cidr_2, ec2=None):
    network_1 = ipaddress.ip_network(tunnel_cidr_1)
    network_2 = ipaddress.ip_network(tunnel_cidr_2)
    ft = {'Name': 'tag:Name', 'Values': [vpn_tag_name]}
    fv = {'Name': 'attachment.vpc-id', 'Values': [vpc_id]}
    for conn in ec2.describe_vpn_connections(Filters=[ft])['VpnConnections']:
        id = conn['VpnGatewayId']
        gws = ec2.describe_vpn_gateways(VpnGatewayIds=[id], Filters=[fv])['VpnGateways']
        if len(gws) <= 0: # the vpn connection may belong to another vpc
           continue
        root = ET.fromstring(conn['CustomerGatewayConfiguration'])
        return check_inside_tunnels(root, 'vpn_gateway', network_1, network_2)
    return False
    
@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_vpn_connection_tunnels(vpc_id, 'RHEDcloudVpnConnection1', '169.254.10.0/30', '169.254.251.248/30')
