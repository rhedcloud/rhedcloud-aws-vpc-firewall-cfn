'''
----------------------------
test_rhedcloud_vpn2_cgw.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn2_cgw",
"Description": "Verify the cgw in the VPN setup matches the cgw for the AWS Research VPC VPN Endpoint 2.",
"Plan": "Describe the VPN connection connecting to AWS Research VPC VPN Endpoint 2, by using tags, and check cgw.",
"ExpectedResult": "Success"

'''

import pytest
from aws_test_functions import aws_client, get_vpc_ids
from tests.structural.test_rhedcloud_vpn1_cgw import check_vpn_connection_cgw

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_vpn_connection_cgw(vpc_id, 'RHEDcloudVpnConnection2', 'RHEDcloud2CustomerGateway')
