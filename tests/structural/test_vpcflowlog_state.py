'''
--------------------
test_vpcflowlog_state.py
--------------------

"Type": "structural",
"Name": "test_vpcflowlog_state",
"Description": "Verify the status of logs delivery.",
"Plan": "Describe the vpc flow logs (need vpc-id) and verify the FlowLogStatus is 'ACTIVE'.",
"ExpectedResult": "Success"

This test loops through all of the VPCs of a particular type (specified as a parameter that is passed in) and checks to make sure flow log state is active for each VPC.

'''

import pytest

from aws_test_functions import get_flow_log_attr, get_vpc_ids


@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    traffic_type = get_flow_log_attr(vpc_id, 'FlowLogStatus')
    assert traffic_type == 'ACTIVE', 'Flow log status is not ACTIVE for VPC {}'.format(vpc_id)
