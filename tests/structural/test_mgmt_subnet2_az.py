'''
----------------------------------
test_mgmt_subnet2_az.py
----------------------------------
"Type": "structural",
"Name": "test_mgmt_subnet2_az",
"Description": "Check to make sure the subnet is in the correct availability zone.",
"Plan": "Verify the 'MGMT2' subnet's availability zone is in the correct region and
         its availability zone is different from the 'MGMT1' subnet's availability zone.
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import check_subnet_az, get_vpc_ids


@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_subnet_az(vpc_id, 'Mgmt2', 'Mgmt1', 'us-east-1')
