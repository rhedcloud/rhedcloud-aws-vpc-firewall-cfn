'''
-------------------------
test_mgmt_subnet2_name.py
-------------------------
"Type": "structural",
"Name": "test_mgmt_subnet2_name",
"Description": "Verify the subnet is named correctly.",
"Plan": "Verify there is a subnet named 'MGMT2' in each RHEDcloud Firewall VPC
         in the account.,
"ExpectedResult": "Success"


'''

import pytest

from aws_test_functions import check_subnet_name, get_vpc_ids


@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_subnet_name(vpc_id, 'Mgmt2')
