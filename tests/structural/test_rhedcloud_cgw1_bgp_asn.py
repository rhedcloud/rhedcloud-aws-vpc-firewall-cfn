'''
-----------------
test_rhedcloud_cgw1_bgp_asn.py
-----------------
                       
"Type": "structural",
"Name": "test_rhedcloud_cgw1_bgp_asn",
"Description": "Verify the BgpAsn is set to the correct value.",
"Plan": "Describe the customer gateway connecting to AWS Research VPC VPN Endpoint 1, by using tags, and check BgpAsn.",
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters


@aws_client('ec2')
def check_rhedcloud_cgw_bgpasn(cgw_tag_name, bgp_asn, ec2=None):
    """Verify that the BGP ASN has been correctly set for the passed in CGW located by tag"""
    
    cgw_filter = dict_to_filters({
        'tag:Name': cgw_tag_name,
    })
    cgw_dict = ec2.describe_customer_gateways(Filters=cgw_filter)
    ''' Result should be false if the CGW tag is not found '''
    if not cgw_dict['CustomerGateways']:
        return False
    for cgw in cgw_dict['CustomerGateways']:
        if cgw['BgpAsn'] != bgp_asn:
            return False
    return True

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_rhedcloud_cgw_bgpasn('RHEDcloud1CustomerGateway','3512')
