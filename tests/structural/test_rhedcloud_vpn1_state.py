'''
----------------------------
test_rhedcloud_vpn1_state.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn1_state",
"Description": "Verify the vpn state is available.",
"Plan": "Describe the VPN connecting to AWS Research VPC VPN Endpoint 1, by using tags, and check state.",
"ExpectedResult": "Success"
'''

import pytest
from aws_test_functions import aws_client, get_vpc_ids

@aws_client('ec2')
def check_vpn_connection_state(vpc_id, tag_name, ec2=None):
    ft = {'Name': 'tag:Name', 'Values': [tag_name]}
    fv = {'Name': 'attachment.vpc-id', 'Values': [vpc_id]}
    for conn in ec2.describe_vpn_connections(Filters=[ft])['VpnConnections']:
        id = conn['VpnGatewayId']
        gws = ec2.describe_vpn_gateways(VpnGatewayIds=[id], Filters=[fv])['VpnGateways']
        if len(gws) <= 0: # the vpn connection may belong to another vpc
           continue
        if conn['State'] != 'available':
            return False
        for a in gws[0]['VpcAttachments']:
            if a['State'] != 'attached':
                return False
        return True
    return False

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_vpn_connection_state(vpc_id, 'RHEDcloudVpnConnection1')
