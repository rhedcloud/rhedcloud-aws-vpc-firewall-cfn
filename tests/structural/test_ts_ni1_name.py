'''
-------------------------
test_ts_ni1_name.py
-------------------------

"Type": "structural",
"Name": "test_ts_ni1_name",
"Description": "Verify the network interface is named correctly.",
"Plan": "Describe the network interface, by using tags, and check to see if the name tag is correct, 'PaFw1TrustNetworkInterface'.",
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters

@aws_client('ec2')
def check_network_interface_name(vpc_id, ni_tag_name, ec2=None):
    filter_ni = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': ni_tag_name,
    })
    dict_ni = ec2.describe_network_interfaces(Filters=filter_ni)
    
    if not dict_ni['NetworkInterfaces']:
        return False

    return True

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_network_interface_name(vpc_id, 'PaFw1TrustNetworkInterface')
