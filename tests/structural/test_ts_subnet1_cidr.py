'''
-------------------------
test_ts_subnet1_cidr.py
-------------------------
"Type": "structural",
"Name": "test_ts_subnet1_cidr",
"Description": "Check to make sure the CIDR space assigned for the subnet is correct.",
"Plan": Verify the 'Trusted1' subnets assigned to each RHEDcloud Type1, Type2 or Firewall VPC
        in the account is part of the primary CIDR block assigned to the VPC.
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import check_subnet_cidr, get_vpc_ids


@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_subnet_cidr(vpc_id, 'Trusted1')
