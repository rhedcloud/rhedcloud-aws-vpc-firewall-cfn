'''
----------------------------
test_vgw_asn.py
----------------------------

"Type": "structural",
"Name": "test_vgw_asn",
"Description": "Verify the AmazonSideAsn of the VPN gateway has the value of 65533.",
"Plan": "Describe the VPN gateways for all of the Firewall VPCs and check to see if their AmazonSideAsn have the same value of 65533.",
"ExpectedResult": "Success"
'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids

@aws_client('ec2')
def check_vgw_asn(vpc_id, asn, ec2=None):
    fv = {'Name': 'attachment.vpc-id', 'Values': [vpc_id]}
    fa = {'Name': 'amazon-side-asn', 'Values': [str(asn)]}
    vpnGateways = ec2.describe_vpn_gateways(Filters=[fv, fa])['VpnGateways']
    return len(vpnGateways) > 0

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_vgw_asn(vpc_id, 65533)
