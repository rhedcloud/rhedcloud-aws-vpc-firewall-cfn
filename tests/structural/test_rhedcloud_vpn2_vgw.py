"""
----------------------------
test_rhedcloud_vpn2_cgw.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn2_vgw",
"Description": "Verify the vgw in the VPN setup matches the vgw for the VPC.",
"Plan": "Describe the VPN connection connecting to AWS Research VPC VPN Endpoint 2, by using tags, and check vgw.",
"ExpectedResult": "Success"

"""

import pytest
from aws_test_functions import aws_client, get_vpc_ids
from tests.structural.test_rhedcloud_vpn1_vgw import check_vpn_connection_vgw


@pytest.mark.parametrize("vpc_id", get_vpc_ids("Firewall"))
def test_answer(vpc_id):
    assert check_vpn_connection_vgw(vpc_id, "RHEDcloudVpnConnection2")
