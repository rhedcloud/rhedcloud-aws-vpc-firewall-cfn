'''
----------------------------------
test_s3endpoint_routetables.py
----------------------------------

"Type": "structural",
"Name": "test_s3endpoint_routetables",
"Description": "Verify the vpc endpoint is associated with correct route tables.",
"Plan": "Describe the vpc endpoint (need vpc-id and service-name) and verify there are three route tables associated with the endpoint.",
"ExpectedResult": "Success"

Verify the s3 endpoint contains one RouteTableIds.  Then check
to make sure that Route Table has a route for a VPC
endpoint (by matching to 'vpce-')

Returns true if endpoint contains 1 RouteTableIds and each of those
route tables has a route for a VPC endpoint.

Copied from VPC type 1 tests - RBH
'''

import pytest

from aws_test_functions import aws_client, dict_to_filters, get_vpc_ids


@aws_client('ec2')
def check_s3endpoint_routetables(vpc_id, *, ec2=None):
    endpoint = ec2.describe_vpc_endpoints(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'service-name': 'com.amazonaws.us-east-1.s3',
    }))
    routeTableIds = endpoint['VpcEndpoints'][0]['RouteTableIds']
    

    if len(routeTableIds) != 3:
        return False
    
    endpoint_id = endpoint['VpcEndpoints'][0]['VpcEndpointId']

    vpceRouteMatchIndex = 0
    for routeTableId in routeTableIds:
        routetables = ec2.describe_route_tables(RouteTableIds=[routeTableId])
        routes = routetables['RouteTables'][0]['Routes']

        for route in routes:
            if route['GatewayId'] == endpoint_id:
                vpceRouteMatchIndex += 1

    if vpceRouteMatchIndex != 3:
        return False

    return True


@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_s3endpoint_routetables(vpc_id)
