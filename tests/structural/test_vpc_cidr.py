'''
----------------
test_vpc_cidr.py
----------------

"Type": "structural",
"Name": "test_vpc_cidr",
"Description": "Check to make sure CIDR space is within RHEDcloud defined space.",
"Plan": "Describe the VPC (need VPC ID) and filter on CidrBlock.  Expected result should be in one of 10.64.0.0/16, 10.65.0.0/16, 10.66.0.0/16, 10.67.0.0/16 and 10.68.0.0/16",
"ExpectedResult": "Success"

Verify the CIDR space assigned to Firewall VPC
in the account is from the known blocks of RHEDcloud Cloud CIDRs.
Returns True if assigned CIDR is inside one of the RHEDcloud Cloud CIDR spaces.

This test only tests the initial CIDR space assigned.  It does not
test any secondary CIDRs added after initial deployment.
'''

import ipaddress

import pytest

from aws_test_functions import get_vpc_cidr, get_vpc_ids


RHEDCLOUD_CLOUD_CIDRS = [
    ipaddress.ip_network(cidr)
    for cidr in ('10.64.0.0/16', 
                 '10.65.0.0/16',
                 '10.66.0.0/16',
                 '10.67.0.0/16',
                 '10.68.0.0/16')
]

def check_cidr(vpc_cidr_network):
    for cidr in RHEDCLOUD_CLOUD_CIDRS:
        if vpc_cidr_network.overlaps(cidr):
            return True
    return False

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    vpc_cidr = get_vpc_cidr(vpc_id)
    vpc_cidr_network = ipaddress.ip_network(vpc_cidr)
    
    assert check_cidr(vpc_cidr_network)
