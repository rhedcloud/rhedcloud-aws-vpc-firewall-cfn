'''
-----------------
test_rhedcloud_cgw2_ipaddress.py
-----------------

"Type": "structural",
"Name": "test_rhedcloud_cgw2_ipaddress",
"Description": "Verify the cgw IP address is that of AWS Research VPC VPN Endpoint 2.",
"Plan": "Describe the customer gateway connecting to AWS Research VPC VPN Endpoint 2, by using tags, and check IP address.",
"ExpectedResult": "Success"


'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters


@aws_client('ec2')
def find_rhedcloud_cgw_ipaddress(cgw_tag_name, ip_address, ec2=None):
    """Function to describe CGW state. If the state is 'available' then its successful."""

    cgw_dict = ec2.describe_customer_gateways(Filters=dict_to_filters({
        'tag:Name': cgw_tag_name,
    }))
    ''' Result should be false if the CGW tag is not found '''
    if not cgw_dict['CustomerGateways']:
        return False
    for cgw in cgw_dict['CustomerGateways']:
        if ip_address not in cgw['IpAddress']:
            return False
    return True

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert find_rhedcloud_cgw_ipaddress('RHEDcloud2CustomerGateway','170.140.77')