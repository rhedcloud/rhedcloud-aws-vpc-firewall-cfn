'''
----------------------------
test_sg_wideopen_ingress.py
----------------------------

"Type": "structural",
"Name": "test_sg_wideopen_ingress",
"Description": "Check to make sure the CIDR space and IP Protocol assigned to the security group for ingress is correct.",
"Plan": "Describe the security group, by using tags, and filter on CidrBlock and IP protocol.",
"ExpectedResult": "Success"

'''

import pytest
import ipaddress
import xml.etree.ElementTree as ET

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters

@aws_client('ec2')
def check_ingress(vpc_id, sg_tag_name, cidr_range, ip_protocol, ec2=None):
    filter_sg = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': sg_tag_name,
        'ip-permission.cidr' : cidr_range,
        'ip-permission.protocol' : ip_protocol,
    })
    dict_sg = ec2.describe_security_groups(Filters=filter_sg)
    
    if not dict_sg['SecurityGroups']:
        return False
    return True
    
@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_ingress(vpc_id, 'sgWideOpen', '0.0.0.0/0', '-1')
