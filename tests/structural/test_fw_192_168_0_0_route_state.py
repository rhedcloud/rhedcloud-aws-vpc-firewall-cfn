'''
------------------------
test_fw_192_168_0_0_route_state.py
------------------------

"Type": "structural",
"Name": "test_fw_192_168_0_0_route_state",
"Description": "Verify the 192.168.0.0/16 route exists and is active for the firewall route table.",
"Plan": "Describe the route table (need route table id) and check to see if default route is present and is active.",
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids, get_vpc_route_table

@aws_client('ec2')
def check_route_state(vpc_id, route_table_name, route_cidr,  ec2=None):
    route_table = get_vpc_route_table(vpc_id, route_table_name, ec2=ec2)
    
    found_route_flag = False
    for route in route_table['Routes']:
        if 'DestinationCidrBlock' not in route:
            continue
        
        if route['DestinationCidrBlock'] == route_cidr:
            found_route_flag = True
        
        if route['DestinationCidrBlock'] == route_cidr and route['State'] != 'active':
            return False

    if found_route_flag == False:
        return False   

    return True


@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
@pytest.mark.parametrize('route_table_name', ['Firewall Mgmt Route Table'])
def test_answer(vpc_id, route_table_name):
    assert check_route_state(vpc_id, route_table_name, '192.168.0.0/16' )
