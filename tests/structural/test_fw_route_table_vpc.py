'''
------------------
test_fw_route_table_vpc.py
------------------

"Name": "test_fw_route_table_vpc",
"Description": "Verify the route table is associated with the correct VPC.",
"Plan": "Describe the route table (need route-table-id) and check to see if the VPC matches the VpcId created by this template.",
"ExpectedResult": "Success"

Verify the route table is associated with the correct VPC.
'''

import pytest

from aws_test_functions import get_vpc_ids, aws_client

@aws_client('ec2')
def check_route_table(vpc_id, tag_name, ec2=None):
    fv = {'Name': 'vpc-id', 'Values': [vpc_id]}
    ft = {'Name': 'tag:Name', 'Values': [tag_name]}
    routeTables = ec2.describe_route_tables(Filters=[fv, ft])['RouteTables']
    return len(routeTables) > 0

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
@pytest.mark.parametrize('route_table_name', ['Firewall Mgmt Route Table', 'Firewall Trust Route Table', 'Firewall Internet Route Table'])
def test_answer(vpc_id, route_table_name):
    assert check_route_table(vpc_id, route_table_name)
