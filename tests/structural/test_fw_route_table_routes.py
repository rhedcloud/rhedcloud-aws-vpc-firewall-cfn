'''
------------------
test_fw_route_table_routes.py
------------------

"Type": "structural",
"Name": "test_fw_route_table_routes",
"Description": "Verify there are three active routes in the route table and check to make sure they are the right ones.",
"Plan": "Describe the route table (need subnet ID) and verify the routes are correct (local - desitination CIDR block matches VPC CIDR block & GatewayId=local, default - desination CIDR Block is 0.0.0.0/0 & GatewayId matches VGW or IGW, and S3 endpoint - desitnation prefix begins with 'pl-') and active.",
"ExpectedResult": "Success"

Verify there are four types of active routes in the route table and check to make sure they are the right ones.
'''

import pytest
from aws_test_functions import get_vpc_ids, aws_client

@aws_client('ec2')
def check_route_table_routes(vpc_id, tag_name, ec2=None):
    fv = {'Name': 'vpc-id', 'Values': [vpc_id]}
    ft = {'Name': 'tag:Name', 'Values': [tag_name]}
    if tag_name == 'Firewall Mgmt Route Table':
        for rt in ec2.describe_route_tables(Filters=[fv, ft])['RouteTables']:
            hasLocal = False
            hasVgw = False
            hasIgw = False
            hasEndpoint = False
            for r in rt['Routes']:
                if r['GatewayId'] == 'local':
                    if r['State'] == 'active':
                        hasLocal = True
                    else:
                        hasLocal = False
                        break
                elif r['GatewayId'].startswith('vgw-'):
                    if r['State'] == 'active':
                        hasVgw = True
                    else:
                        hasVgw = False
                        break
                elif r['GatewayId'].startswith('igw-'):
                    if r['State'] == 'active':
                        hasIgw = True
                    else:
                        hasIgw = False
                        break
                elif r['GatewayId'].startswith('vpc'):
                    if r['DestinationPrefixListId'].startswith('pl-'):
                        if r['State'] == 'active':
                            hasEndpoint = True
                        else:
                            hasEndpoint = False
                            break
            return hasLocal and hasVgw and hasIgw and hasEndpoint 
        return False
    else:
        for rt in ec2.describe_route_tables(Filters=[fv, ft])['RouteTables']:
            hasLocal = False
            hasIgw = False
            hasEndpoint = False
            for r in rt['Routes']:
                if r['GatewayId'] == 'local':
                    if r['State'] == 'active':
                        hasLocal = True
                    else:
                        hasLocal = False
                        break
                elif r['GatewayId'].startswith('igw-'):
                    if r['State'] == 'active':
                        hasIgw = True
                    else:
                        hasIgw = False
                        break
                elif r['GatewayId'].startswith('vpc'):
                    if r['DestinationPrefixListId'].startswith('pl-'):
                        if r['State'] == 'active':
                            hasEndpoint = True
                        else:
                            hasEndpoint = False
                            break
            return hasLocal and hasIgw and hasEndpoint 
        return False

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
@pytest.mark.parametrize('route_table_name', ['Firewall Mgmt Route Table', 'Firewall Trust Route Table', 'Firewall Internet Route Table'])
def test_answer(vpc_id, route_table_name):
    assert check_route_table_routes(vpc_id, route_table_name)
