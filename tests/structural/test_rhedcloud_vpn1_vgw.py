'''
----------------------------
test_rhedcloud_vpn1_cgw.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn1_vgw",
"Description": "Verify the vgw in the VPN setup matches the vgw for the VPC.",
"Plan": "Describe the VPN connection connecting to AWS Research VPC VPN Endpoint 1, by using tags, and check vgw.",
"ExpectedResult": "Success"

'''

import pytest
from aws_test_functions import aws_client, get_vpc_ids

@aws_client('ec2')
def check_vpn_connection_vgw(vpc_id, tag_name, ec2=None):
    ft = {'Name': 'tag:Name', 'Values': [tag_name]}
    fv = {'Name': 'attachment.vpc-id', 'Values': [vpc_id]}
    for conn in ec2.describe_vpn_connections(Filters=[ft])['VpnConnections']:
        id = conn['VpnGatewayId']
        gws = ec2.describe_vpn_gateways(VpnGatewayIds=[id], Filters=[fv])['VpnGateways']
        if len(gws) <= 0: # the vpn connection may belong to another vpc
           continue
        return True
    return False

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_vpn_connection_vgw(vpc_id, 'RHEDcloudVpnConnection1')
