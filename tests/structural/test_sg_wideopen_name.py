'''
-------------------------
test_sg_wideopen_name.py
-------------------------

"Type": "structural",
"Name": "test_sg_wideopen_name",
"Description": "Verify the security group is named correctly.",
"Plan": "Describe the security group, by using tags, and check to see if the name tag is correct, 'sgWideOpen'.",
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters

@aws_client('ec2')
def check_security_group_name(vpc_id, sg_tag_name, ec2=None):
    filter_sg = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': sg_tag_name,
    })
    dict_sg = ec2.describe_security_groups(Filters=filter_sg)
    
    if not dict_sg['SecurityGroups']:
        return False

    return True

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_security_group_name(vpc_id, 'sgWideOpen')
