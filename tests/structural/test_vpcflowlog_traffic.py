'''
--------------------
test_vpcflowlog_traffic.py
--------------------

"Type": "structural",
"Name": "test_vpcflowlog_traffic",
"Description": "Verify vpc flow logs is setup for all traffic (accepts and rejects).",
"Plan": "Describe the vpc flow logs (need vpc-id) and verify 'TrafficType' equals 'ALL'.",
"ExpectedResult": "Success"

This test loops through all of the VPCs of a particular type (specified as a parameter that is passed in) and checks to make sure the traffic type is set to ALL for each VPC.

'''

import pytest

from aws_test_functions import get_flow_log_attr, get_vpc_ids


@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    traffic_type = get_flow_log_attr(vpc_id, 'TrafficType')
    assert traffic_type == 'ALL', 'Flow log traffic type is not ALL for VPC {}'.format(vpc_id)
