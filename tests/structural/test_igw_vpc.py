'''
----------------------------
test_igw_vpc.py
----------------------------

"Type": "structural",
"Name": "test_igw_vpc",
"Description": "Verify the Internet Gateway State is attached to VPC",
"Plan": "Describe the Internet gateway and check that it is attached to the expected vpc.",
"ExpectedResult": "Success"
'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters

@aws_client('ec2')
def get_igw_vpc(vpc_id, ec2=None):
    """Check to find IGW attached to VPC"""
    
    igw_dict = ec2.describe_internet_gateways(
        Filters=dict_to_filters({
            'attachment.vpc-id': vpc_id,
        })
    )
        
    if not igw_dict['InternetGateways']:
        return False
    for igw in igw_dict['InternetGateways']:
        if igw['Attachments'][0]['VpcId'] != vpc_id:
            return False
        else:
            return True
    
@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert get_igw_vpc(vpc_id)