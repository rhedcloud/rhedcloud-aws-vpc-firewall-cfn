'''
--------------------------
test_mgmt_subnet2_state.py
--------------------------
"Type": "structural",
"Name": "test_mgmt_subnet2_state",
"Description": "Verify the subnet is available by checking its status.",
"Plan": Verify the 'MGMT2 subnets assigned to each RHEDcloud Type1, Type2 or Firewall VPC
        in the account has a state of available.
"ExpectedResult": "Success"



'''

import pytest

from aws_test_functions import check_subnet_state, get_vpc_ids


@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_subnet_state(vpc_id, 'Mgmt2')
