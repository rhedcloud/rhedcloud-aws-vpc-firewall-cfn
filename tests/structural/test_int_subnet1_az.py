'''
----------------------------------
test_int_subnet1_az.py
----------------------------------
"Type": "structural",
"Name": "test_int_subnet1_az",
"Description": "Check to make sure the subnet is in the correct availability zone.",
"Plan": "Verify the 'Internet1' subnet's availability zone is in the correct region and
         its availability zone is different from the 'Internet2' subnet's availability zone.
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import check_subnet_az, get_vpc_ids


@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_subnet_az(vpc_id, 'Internet1', 'Internet2', 'us-east-1')
