'''
-------------------------
test_mgmt_ni1_subnet.py
-------------------------

"Type": "structural",
"Name": "test_mgmt_ni1_subnet",
"Description": "Check to make sure the subnet assigned to the network interface is correct.",
"Plan": "Describe the network interface, by using tags, and filter on subnet.",
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters

@aws_client('ec2')
def check_network_interface_subnet(vpc_id, ni_tag_name, ni_subnet_tag_name, ec2=None):
    # Get subnetID by using the logical-id.   
    filter_subnet = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:aws:cloudformation:logical-id': ni_subnet_tag_name,
    })
    dict_subnet = ec2.describe_subnets(Filters=filter_subnet)['Subnets']
    
    if not dict_subnet:
        return False
    
    # Get the network interface 
    filter_ni = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': ni_tag_name,
        'subnet-id' : dict_subnet[0]['SubnetId']
    })
    dict_ni = ec2.describe_network_interfaces(Filters=filter_ni)
    
    if not dict_ni['NetworkInterfaces']:
        return False

    return True

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_network_interface_subnet(vpc_id, 'PaFw1MgmtNeworkInterface', 'MgmtSubnet1')

