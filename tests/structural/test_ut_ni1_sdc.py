'''
-------------------------
test_ut_ni1_sdc.py
-------------------------

"Type": "structural",
"Name": "test_ut_ni1_sdc",
"Description": "Check to make sure the source destination check on the network interface is correct.",
"Plan": "Describe the network interface, by using tags, and filter on source destination check.",
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters

@aws_client('ec2')
def check_network_interface_sdc(vpc_id, ni_tag_name, src_dest_check, ec2=None):
    filter_ni = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': ni_tag_name,
        'source-dest-check' : src_dest_check,
    })
    dict_ni = ec2.describe_network_interfaces(Filters=filter_ni)
    
    if not dict_ni['NetworkInterfaces']:
        return False
    
    return True

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_network_interface_sdc(vpc_id, 'PaFw1InternetNetworkInterface', 'false')
