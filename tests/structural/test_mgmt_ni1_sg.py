'''
-------------------------
test_mgmt_ni1_sg.py
-------------------------

"Type": "structural",
"Name": "test_mgmt_ni1_sg",
"Description": "Check to make sure the security group assigned to the network interface is correct.",
"Plan": "Describe the security group, by using tags, and filter on security group.",
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters

@aws_client('ec2')
def check_network_interface_sg(vpc_id, ni_tag_name, sg_tag_name, ec2=None):
    # Get security group ID by using the logical-id.   
    filter_sg = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:aws:cloudformation:logical-id': sg_tag_name,
    })
    dict_sg = ec2.describe_security_groups(Filters=filter_sg)['SecurityGroups']
    
    if not dict_sg:
        return False
    
    # Get the network interface 
    filter_ni = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': ni_tag_name,
        'group-id' : dict_sg[0]['GroupId']
    })
    dict_ni = ec2.describe_network_interfaces(Filters=filter_ni)
    
    if not dict_ni['NetworkInterfaces']:
        return False

    return True

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_network_interface_sg(vpc_id, 'PaFw1MgmtNeworkInterface', 'sgManagement')
