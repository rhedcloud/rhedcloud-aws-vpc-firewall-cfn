'''
----------------------------
test_sg_wideopen_egress.py
----------------------------

"Type": "structural",
"Name": "test_sg_wideopen_egress",
"Description": "Check to make sure the CIDR space and IP Protocol assigned to the security group for egress is correct.",
"Plan": "Describe the security group, by using tags, and filter on CidrBlock and IP protocol.",
"ExpectedResult": "Success"

'''

import pytest
import ipaddress
import xml.etree.ElementTree as ET

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters

@aws_client('ec2')
def check_egress(vpc_id, sg_tag_name, cidr_range, ip_protocol, ec2=None):
    filter_sg = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': sg_tag_name,
        'egress.ip-permission.cidr' : cidr_range,
        'egress.ip-permission.protocol' : ip_protocol,
    
    })
    dict_sg = ec2.describe_security_groups(Filters=filter_sg)

    if not dict_sg['SecurityGroups']:
        return False
    return True
    
@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_egress(vpc_id, 'sgWideOpen', '0.0.0.0/0', '-1')

