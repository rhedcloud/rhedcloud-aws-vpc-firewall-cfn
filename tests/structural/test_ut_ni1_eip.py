'''
-------------------------
test_ut_ni1_eip.py
-------------------------

"Type": "structural",
"Name": "test_ut_ni1_eip",
"Description": "Check to make sure an Elastic IP has been assigned to the the network interface.",
"Plan": "Describe the network interface, by using tags, and verify that a EIP has been associated and assigned",
"ExpectedResult": "Success"

'''

import pytest

from aws_test_functions import aws_client, get_vpc_ids, dict_to_filters

@aws_client('ec2')
def check_network_interface_sg(vpc_id, ni_tag_name, ec2=None):
    # Get the network interface 
    filter_ni = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': ni_tag_name,
    })
    dict_ni = ec2.describe_network_interfaces(Filters=filter_ni)['NetworkInterfaces']
    if not dict_ni:
        return False

    # Get the EIP 
    filter_eip = dict_to_filters({
        'allocation-id': dict_ni[0]['Association']['AllocationId'],
        'association-id': dict_ni[0]['Association']['AssociationId'],
    })
    dict_eip = ec2.describe_addresses(Filters=filter_eip)
    if not dict_eip['Addresses']:
        return False
    
    return True

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_network_interface_sg(vpc_id, 'PaFw1InternetNetworkInterface')
