'''
----------------------------
test_rhedcloud_vpn2_tunnel.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn2_tunnel",
"Description": "Verify the tunnnel inside cidr in the VPN setup.",
"Plan": "Describe the VPN connection connecting to AWS Research VPC VPN Endpoint 2, by using tags, and check tunnnel inside cidr.",
"ExpectedResult": "Success"
'''

import pytest
import ipaddress
import xml.etree.ElementTree as ET
from aws_test_functions import aws_client, get_vpc_ids
from ipaddress import ip_network
from tests.structural.test_rhedcloud_vpn1_tunnel import check_vpn_connection_tunnels

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_vpn_connection_tunnels(vpc_id, 'RHEDcloudVpnConnection2', '169.254.10.8/30', '169.254.255.248/30')
