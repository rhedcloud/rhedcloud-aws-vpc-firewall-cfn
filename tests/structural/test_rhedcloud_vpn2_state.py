'''
----------------------------
test_rhedcloud_vpn2_state.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn2_state",
"Description": "Verify the vpn state is available.",
"Plan": "Describe the VPN connecting to AWS Research VPC VPN Endpoint 2, by using tags, and check state.",
"ExpectedResult": "Success"
'''

import pytest
from aws_test_functions import aws_client, get_vpc_ids
from tests.structural.test_rhedcloud_vpn1_state import check_vpn_connection_state

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_vpn_connection_state(vpc_id, 'RHEDcloudVpnConnection2')
