'''
----------------------------
test_rhedcloud_vpn1_cgw.py
----------------------------

"Type": "structural",
"Name": "test_rhedcloud_vpn1_cgw",
"Description": "Verify the cgw in the VPN setup matches the cgw for the AWS Research VPC VPN Endpoint 1.",
"Plan": "Describe the VPN connection connecting to AWS Research VPC VPN Endpoint 1, by using tags, and check cgw.",
"ExpectedResult": "Success"

'''

import pytest
from aws_test_functions import aws_client, get_vpc_ids

@aws_client('ec2')
def check_vpn_connection_cgw(vpc_id, vpn_tag_name, cgw_tag_name, ec2=None):
    fc = {'Name': 'tag:Name', 'Values': [cgw_tag_name]}
    ft = {'Name': 'tag:Name', 'Values': [vpn_tag_name]}
    fv = {'Name': 'attachment.vpc-id', 'Values': [vpc_id]}
    for conn in ec2.describe_vpn_connections(Filters=[ft])['VpnConnections']:
        id = conn['VpnGatewayId']
        gws = ec2.describe_vpn_gateways(VpnGatewayIds=[id], Filters=[fv])['VpnGateways']
        if len(gws) <= 0: # the vpn connection may belong to another vpc
           continue
        cid = conn['CustomerGatewayId']
        cgws = ec2.describe_customer_gateways(CustomerGatewayIds=[cid], Filters=[fc])['CustomerGateways']
        return len(cgws) > 0
    return False

@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    assert check_vpn_connection_cgw(vpc_id, 'RHEDcloudVpnConnection1', 'RHEDcloud1CustomerGateway')
