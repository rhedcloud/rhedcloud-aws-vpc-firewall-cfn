'''
----------------
test_vpc_type.py
----------------

"Type": "structural",
"Name": "test_vpc_type",
"Description": "Check to make sure RHEDcloudVpcType tag is Firewall.",
"Plan": "Describe the VPC (need VPC ID) and filter on RHEDcloudVpcType tag.  Expected result should be 1.",
"ExpectedResult": "Success"

Verify all the Firewall VPCs in the account have an RHEDcloudVpcType of 'Firewall'.
Returns True if all Firewall VPCs in the account have an RHEDcloudVpcType of 'Firewall'.
'''

import boto3
import pytest

from aws_test_functions import get_vpc_ids


@pytest.mark.parametrize('vpc_id', get_vpc_ids('Firewall'))
def test_answer(vpc_id):
    ec2 = boto3.client('ec2')
    response = ec2.describe_vpcs(VpcIds=[vpc_id])

    for tag in response['Vpcs'][0]['Tags']:
        if tag['Key'] != 'RHEDcloudVpcType':
            continue

        assert tag['Value'] == 'Firewall'
