# rhedcloud-aws-vpc-firewall-cfn

This implements the RHEDcloud firewall VPC and firewall infrastructure, presently implemented with Palo Alto Networks.

## Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

## License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt
